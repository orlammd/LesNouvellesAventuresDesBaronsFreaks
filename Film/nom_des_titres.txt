Chapitre 1: préambule
Chapitre 2: l'idée lumineuse de Jack Caesar pour établir la paix publicitaire
Chapitre 3: la persistance éclairée de Jack Caesar pour vraiment rétablir la paix publicitaire
Chapitre 4: la décisive main baladeuse du brillant bras-droit de Jack Caesar pour dévinitivement vraiment rétablir la paix publicitaire

